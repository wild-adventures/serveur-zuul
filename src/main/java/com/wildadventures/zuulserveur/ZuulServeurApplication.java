package com.wildadventures.zuulserveur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuulServeurApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulServeurApplication.class, args);
	}
}
